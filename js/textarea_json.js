/**
 * @file
 * Defines Javascript behaviors for text areas displaying JSON data.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Behavior description.
   */
  Drupal.behaviors.unomiConnect = {
    attach: function (context, settings) {
      var jsonObj = JSON.parse($('#json', context).val());
      $('#json', context).val(JSON.stringify(jsonObj, undefined, 4));
    }
  };

}(jQuery, Drupal, drupalSettings));
