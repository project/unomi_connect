<?php

namespace Drupal\unomi_rules\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure unomi_rules settings for this site.
 */
class UnomiRulesSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unomi_rules_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['unomi_rules.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['rules_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rules URI'),
      '#default_value' => $this->config('unomi_rules.settings')->get('rules_uri'),
      '#field_prefix' => $this->config('unomi_connect.settings')->get('base_uri'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('unomi_rules.settings')
      ->set('rules_uri', $form_state->getValue('rules_uri'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
