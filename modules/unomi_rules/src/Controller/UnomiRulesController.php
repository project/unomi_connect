<?php

namespace Drupal\unomi_rules\Controller;

use Drupal\unomi_connect\Controller\UnomiControllerBase;

/**
 * Returns responses for unomi_rules routes.
 */
class UnomiRulesController extends UnomiControllerBase {

  /**
   * Builds the response.
   */
  public function index() {

    $rules = $this->getContents(
      $this->config('unomi_rules.settings')->get('rules_uri')
    );

    $header = [
      ['data' => $this->t('#')],
      ['data' => $this->t('Name')],
      ['data' => $this->t('Description')],
      ['data' => $this->t('Scope')],
      ['data' => $this->t('Status')],
      ['data' => $this->t('Operations')],
    ];

    $rows = [];
    foreach ($rules as $rule) {
      $rows[] = [
        'data' => [
          'id' => $rule->id,
          'name' => $rule->name,
          'description' => $rule->description,
          'scope' => $rule->scope,
          'status' => $rule->enabled ? 'Enabled' : 'Disabled',
          'operations' => '---',
        ],
      ];
    }

    $output['table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      'pager' => [
        '#type' => 'pager',
      ],
    ];

    return $output;
  }

  /**
   * {@inheritDoc}
   */
  public function getModuleName() : string {
    return 'Unomi Rules';
  }

  /**
   * {@inheritDoc}
   */
  public function getSettingsFormRoute() : string {
    return 'unomi_rules.settings_form';
  }

}
