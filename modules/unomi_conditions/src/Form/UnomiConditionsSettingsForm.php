<?php

namespace Drupal\unomi_conditions\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Unomi Conditions settings for this site.
 */
class UnomiConditionsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unomi_conditions_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['unomi_conditions.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['conditions_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Conditions URI'),
      '#default_value' => $this->config('unomi_conditions.settings')->get('conditions_uri'),
      '#field_prefix' => $this->config('unomi_connect.settings')->get('base_uri'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('unomi_conditions.settings')
      ->set('conditions_uri', $form_state->getValue('conditions_uri'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
