<?php

namespace Drupal\unomi_conditions;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\unomi_connect\UnomiConnect;

/**
 * UnomiConditions service.
 */
class UnomiConditions {

  use StringTranslationTrait;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The unomi_connect service.
   *
   * @var \Drupal\unomi_connect\UnomiConnect
   */
  protected $unomiConnect;

  /**
   * Constructs an UnomiConditions object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\unomi_connect\UnomiConnect $unomi_connect
   *   The unomi_connect service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, UnomiConnect $unomi_connect) {
    $this->configFactory = $config_factory;
    $this->messenger = $messenger;
    $this->unomiConnect = $unomi_connect;
  }

  /**
   * Get unomi conditions.
   *
   * @return array
   *   Return array with unomi conditions;
   */
  public function getConditions() : array {
    $uri = $this->configFactory->get('unomi_conditions.settings')->get('conditions_uri');
    try {
      $response = $this->unomiConnect->makeRequest('GET', $uri);
      return json_decode($response->getBody()->getContents());
    }
    catch (\Throwable $th) {
      $this->messenger->addWarning(
        $this->t('Please set the Unomi Conditions uri in <a href="@linkForm">@linkForm</a>.', [
          '@linkForm' => Url::fromRoute('unomi_conditions.settings_form')->toString(),
        ])
      );
      return [];
    }
  }

  /**
   * Method description.
   *
   * @return array
   *   Return comparison operators.
   */
  public function getComparisonOperators() {
    return [
      'all' => 'all',
      'between' => 'between',
      'contains' => 'contains',
      'endsWith' => 'endsWith',
      'equals' => 'equals',
      'exists' => 'exists',
      'greaterThan' => 'greaterThan',
      'greaterThanOrEqualTo' => 'greaterThanOrEqualTo',
      'in' => 'in',
      'isDay' => 'isDay',
      'isNotDay' => 'isNotDay',
      'lessThan' => 'lessThan',
      'lessThanOrEqualTo' => 'lessThanOrEqualTo',
      'matchesRegex' => 'matchesRegex',
      'missing' => 'missing',
      'notEquals' => 'notEquals',
      'notIn' => 'notIn',
      'startsWith' => 'startsWith',
    ];
  }

  /**
   * Get conditions operators.
   *
   * @return array
   *   Return array with operators.
   */
  public function getOperators() {
    return [
      'and' => 'and',
      'or' => 'or',
      'not' => 'not',
    ];
  }

}
