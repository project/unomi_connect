<?php

namespace Drupal\unomi_conditions\Controller;

use Drupal\unomi_connect\Controller\UnomiControllerBase;

/**
 * Returns responses for Unomi Conditions routes.
 */
class UnomiConditionsController extends UnomiControllerBase {

  /**
   * List the conditions.
   */
  public function index() {

    $conditions = $this->getContents(
      $this->config('unomi_conditions.settings')->get('conditions_uri')
    );

    $header = [
      ['data' => $this->t('#')],
      ['data' => $this->t('Name')],
      ['data' => $this->t('Description')],
      ['data' => $this->t('System Tags')],
      ['data' => $this->t('Operations')],
    ];

    $rows = [];
    foreach ($conditions as $condition) {
      $rows[] = [
        'data' => [
          'id' => $condition->id,
          'name' => $condition->name,
          'description' => $condition->description,
          'SystemTags' => implode(', ', $condition->systemTags),
          'operations' => '---',
        ],
      ];
    }

    $output['table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      'pager' => [
        '#type' => 'pager',
      ],
    ];

    return $output;
  }

  /**
   * {@inheritDoc}
   */
  public function getModuleName() : string {
    return 'Unomi Conditions';
  }

  /**
   * {@inheritDoc}
   */
  public function getSettingsFormRoute() : string {
    return 'unomi_conditions.settings_form';
  }

}
