<?php

namespace Drupal\unomi_segments\Controller;

use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\unomi_connect\Controller\UnomiControllerBase;

/**
 * Returns responses for Unomi Segments routes.
 */
class UnomiSegmentsController extends UnomiControllerBase {

  /**
   * Builds the response.
   */
  public function index() {

    $segments = $this->getContents(
      $this->config('unomi_segments.settings')->get('segments_uri')
    );

    $header = [
      ['data' => $this->t('#')],
      ['data' => $this->t('Name')],
      ['data' => $this->t('Description')],
      ['data' => $this->t('Scope')],
      ['data' => $this->t('Tags')],
      ['data' => $this->t('Status')],
      ['data' => $this->t('Operations')],
    ];

    $rows = [];
    asort($segments);
    foreach ($segments as $segment) {
      $operations = [];
      $operations['view'] = [
        'title' => $this->t('View'),
        'url' => Url::fromRoute('unomi_segments.segment', ['segment_id' => $segment->id]),
      ];
      if (\Drupal::moduleHandler()->moduleExists('unomi_profiles')) {
        $operations['profiles'] = [
          'title' => $this->t('List profiles'),
          'url' => Url::fromRoute('unomi_segments.segment.profiles', ['segment_id' => $segment->id]),
        ];
      }
      $operations['delete'] = [
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute('unomi_segments.delete_confirm', ['segmentId' => $segment->id]),
      ];

      $rows[$segment->id] = [
        'class' => ['draggable'],
        'data' => [
          'id' => $segment->id,
          'name' => $segment->name,
          'description' => $segment->description,
          'scope' => $segment->scope,
          'tags' => implode(', ', $segment->tags),
          'status' => $segment->enabled ? 'Enabled' : 'Disabled',
          'edit' => [
            'data' => [
              '#type' => 'operations',
              '#links' => $operations,
            ],
          ],
        ],
      ];
    }

    $output['states'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('No segments found.'),
      '#attributes' => [
        'class' => ['devel-state-list'],
      ],
    ];

    return $output;
  }

  /**
   * Shows a single segment.
   *
   * @param string $segment_id
   *   The segment ID.
   *
   * @param array
   *   The build array.
   */
  public function segment(string $segment_id) {
    $segments_uri = $this->config('unomi_segments.settings')->get('segments_uri');

    $result = $this->unomiConnect->makeRequest('GET', $segments_uri . '/' . $segment_id);
    $segment = $result->getBody()->getContents();

    $build['#title'] = $this->t('Unomi segment %id', [
      '%id' => $segment_id,
    ]);

    $build['segment'] = [
      '#type' => 'unomi_connect_json_data',
      '#data' => $segment,
    ];

    return $build;
  }

  /**
   * Route callback for segment profiles.
   *
   * Expects unomi_profiles module to be present.
   *
   * @param string $segment_id
   *   The segment ID.
   *
   * @param array
   *   The build array.
   */
  public function segmentProfiles(string $segment_id): array {
    $unomiConnect = \Drupal::service('unomi_connect');
    $search = (object) [
      'offset' => 0,
      'forceRefresh' => TRUE,
      'condition' => (object) [
        'type' => 'profilePropertyCondition',
        'parameterValues' => (object) [
          'propertyName' => 'segments',
          'comparisonOperator' => 'contains',
          'propertyValue' => $segment_id,
        ],
      ],
    ];

    $response = $unomiConnect->makeRequest('POST', '/cxs/profiles/search/', $search);

    $profiles_list = $unomiConnect->decodeListResponse($response);

    $build = [];

    $build['#title'] = $this->t('Unomi profiles for segment %id', [
      '%id' => $segment_id,
    ]);

    $build['profiles'] = [
      '#type' => 'unomi_profile_list',
      '#profiles' => $profiles_list,
    ];

    return $build;
  }

  /**
   * {@inheritDoc}
   */
  public function getModuleName() : string {
    return 'Unomi Segments';
  }

  /**
   * {@inheritDoc}
   */
  public function getSettingsFormRoute() : string {
    return 'unomi_segments.settings_form';
  }

}
