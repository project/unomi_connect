<?php

namespace Drupal\unomi_segments\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\unomi_conditions\UnomiConditions;
use Drupal\unomi_connect\UnomiConnect;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements the ajax demo form controller.
 *
 * This example demonstrates using ajax callbacks to add people's names to a
 * list of picnic attendees.
 *
 * @see \Drupal\Core\Form\FormBase
 * @see \Drupal\Core\Form\ConfigFormBase
 */
class UnomiSegmentsForm extends FormBase {

  /**
   * The unomi conditions service.
   *
   * @var \Drupal\unomi_conditions\UnomiConditions
   */
  protected $unomiConditions;

  /**
   * The unomi connect service.
   *
   * @var \Drupal\unomi_connect\UnomiConnect
   */
  protected $unomiConnect;

  /**
   * The constructor.
   *
   * @param \Drupal\unomi_conditions\UnomiConditions $unomi_conditions
   *   The unomi conditions service.
   * @param \Drupal\unomi_connect\UnomiConnect $unomi_connect
   *   The unomi connect service.
   */
  public function __construct(
    UnomiConditions $unomi_conditions,
    UnomiConnect $unomi_connect
  ) {
    $this->unomiConditions = $unomi_conditions;
    $this->unomiConnect = $unomi_connect;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('unomi_conditions'),
      $container->get('unomi_connect'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unomi_segments_segments';
  }

  /**
   * Form with 'add more' and 'remove' buttons.
   *
   * This example shows a button to "add more" - add another textfield, and
   * the corresponding "remove" button.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#attached']['library'][] = 'unomi_connect/unomi_connect';

    // Gather the number of names in the form already.
    $num_subconditions = $form_state->get('num_subconditions');
    // We have to ensure that there is at least one name field.
    if ($num_subconditions === NULL) {
      $name_field = $form_state->set('num_subconditions', 1);
      $num_subconditions = 1;
    }

    $form['id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ID'),
      // '#required' => TRUE,
    ];

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      // '#required' => TRUE,
    ];

    $form['scope'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scope'),
      // '#required' => TRUE,
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      // '#required' => TRUE,
    ];

    $form['readonly'] = [
      '#type' => 'select',
      '#title' => $this->t('Readonly'),
      '#required' => TRUE,
      '#options' => [
        TRUE => $this->t('TRUE'),
        FALSE => $this->t('FALSE'),
      ],
    ];

    $form['#tree'] = TRUE;
    $form['conditions'] = [
      '#type' => 'details',
      '#title' => $this->t('Conditions'),
      '#open' => TRUE,
      '#prefix' => '<div id="names-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['conditions']['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#options' => $this->getOptionsConditions(),
    ];

    $form['conditions']['operator'] = [
      '#type' => 'select',
      '#title' => $this->t('Operator'),
      '#required' => TRUE,
      '#options' => $this->unomiConditions->getOperators(),
    ];

    for ($i = 0; $i < $num_subconditions; $i++) {

      $form['conditions']['subConditions'][$i] = [
        '#type' => 'details',
        '#title' => $this->t('Subconditions'),
        '#open' => TRUE,
      ];

      $form['conditions']['subConditions'][$i]['type'][$i] = [
        '#type' => 'select',
        '#title' => $this->t('Type'),
        '#options' => $this->getOptionsConditions(),
      ];

      $form['conditions']['subConditions'][$i]['propertyName'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('Property Name'),
        // '#required' => TRUE,
      ];

      $form['conditions']['subConditions'][$i]['type'][$i] = [
        '#type' => 'select',
        '#title' => $this->t('Type'),
        '#options' => $this->getOptionsConditions(),
      ];

      $form['conditions']['subConditions'][$i]['comparisonOperator'][$i] = [
        '#type' => 'select',
        '#title' => $this->t('Comparison Operator'),
        // '#required' => TRUE,
        '#options' => $this->unomiConditions->getComparisonOperators(),
      ];

      // Property.
      $form['conditions']['subConditions'][$i]['container'][$i] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['property-wrapper form-item']],
        'label' => [
          '#markup' => '<label><b>' . $this->t('Maximum image dimensions') . '</b></label><br>',
        ],
      ];

      $form['conditions']['subConditions'][$i]['container'][$i]['property'] = [
        '#type' => 'textfield',
        '#placeholder' => $this->t('Property'),
        '#field_suffix' => ':',
      ];

      $form['conditions']['subConditions'][$i]['container'][$i]['propertyValue'] = [
        '#type' => 'textfield',
        '#placeholder' => $this->t('Property value'),
      ];

    }

    $form['conditions']['actions'] = [
      '#type' => 'actions',
    ];
    $form['conditions']['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more subcondition'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'names-fieldset-wrapper',
      ],
    ];
    // If there is more than one name, add the remove button.
    if ($num_subconditions > 1) {
      $form['conditions']['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'names-fieldset-wrapper',
        ],
      ];
    }
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['conditions'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_subconditions');
    $add_button = $name_field + 1;
    $form_state->set('num_subconditions', $add_button);
    // Since our buildForm() method relies on the value of 'num_subconditions'
    // to generate 'name' form elements, we have to tell the form to rebuild.
    // If we don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_subconditions');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_subconditions', $remove_button);
    }

    // Since our buildForm() method relies on the value of 'num_subconditions'
    // to generate 'name' form elements, we have to tell the form to rebuild.
    // If we don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Final submit handler.
   *
   * Reports what values were finally set.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $objSegment = new \stdClass();
    $objSegment->metadata = new \stdClass();
    $objSegment->metadata->id = $form_state->getValue('id');
    $objSegment->metadata->name = $form_state->getValue('name');
    $objSegment->metadata->scope = $form_state->getValue('scope');
    $objSegment->metadata->description = $form_state->getValue('description');
    $objSegment->metadata->readOnly = (bool) $form_state->getValue('readonly');

    // Condition ID.
    $objSegment->condition = new \stdClass();
    $objSegment->condition->type = $form_state->getValue('conditions')['type'];
    $subConditions = $form_state->getValue('conditions')['subConditions'];
    for ($index = 0; $index < count($subConditions); $index++) {
      $sub[] = (object) [
        'parameterValues' => (object) [
          'propertyName' => $subConditions[$index]['propertyName'][$index],
          'comparisonOperator' => $subConditions[$index]['comparisonOperator'][$index],
          $subConditions[$index]['container'][$index]['property'] => $subConditions[$index]['container'][$index]['propertyValue'],
        ],
        'type' => $subConditions[$index]['type'][$index],
      ];
    }
    $objSegment->condition->parameterValues = (object) [
      'subConditions' => $sub,
      'operator' => $form_state->getValue('conditions')['operator'],
    ];

    $this->unomiConnect->makeRequest('POST', $this->config('unomi_segments.settings')->get('segments_uri'), $objSegment);

    $this->messenger()->addMessage($this->t('Segment successfully added.'));
  }

  /**
   * Get the options.
   */
  protected function getOptionsConditions() {
    $optionsConditions = [];

    $conditions = $this->unomiConditions->getConditions();
    foreach ($conditions as $condition) {
      $optionsConditions[$condition->id] = $condition->name;
    }

    return $optionsConditions;
  }

}
