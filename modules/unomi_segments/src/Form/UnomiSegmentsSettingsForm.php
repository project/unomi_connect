<?php

namespace Drupal\unomi_segments\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Unomi Segments settings for this site.
 */
class UnomiSegmentsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unomi_segments_unomi_segments_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['unomi_segments.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['segments_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Segments URI'),
      '#default_value' => $this->config('unomi_segments.settings')->get('segments_uri'),
      '#field_prefix' => $this->config('unomi_connect.settings')->get('base_uri'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('unomi_segments.settings')
      ->set('segments_uri', $form_state->getValue('segments_uri'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
