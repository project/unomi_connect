<?php

namespace Drupal\unomi_segments\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\unomi_connect\UnomiConnect;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirmation form before clearing out the examples.
 */
class DeleteConfirmForm extends ConfirmFormBase {

  /**
   * The Unomi Connect service.
   *
   * @var \Drupal\unomi_connect\UnomiConnect
   */
  protected $unomiConnect;

  /**
   * The construct.
   *
   * @param \Drupal\unomi_connect\UnomiConnect $unomi_connect
   *   The Unomi Connect service.
   */
  public function __construct(UnomiConnect $unomi_connect) {
    $this->unomiConnect = $unomi_connect;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('unomi_connect')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unomi_segments_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $segmentId = $this->getRequest()->get('segmentId');
    return $this->t('Are you sure you want to delete the segment %segmentId?', [
      '%segmentId' => $segmentId,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('unomi_segments.segments');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $uriSegment = $this->config('unomi_segments.settings')->get('segments_uri');
    $segmentId = $this->getRequest()->get('segmentId');
    $uriSegmentDelete = $uriSegment . '/' . $segmentId;

    try {
      $this->unomiConnect->makeRequest('DELETE', $uriSegmentDelete);
      $this->messenger()->addStatus($this->t('Done! Unomi segment @segmentId successfully deleted.', [
        '@segmentId' => $segmentId,
      ]));
    }
    catch (\Throwable $th) {
      $this->messenger()->addError($this->t('Error to delete unomi segment @segmentId!', [
        '@segmentId' => $segmentId,
      ]));
    }

    sleep(1);
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
