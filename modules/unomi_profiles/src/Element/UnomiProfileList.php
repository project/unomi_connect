<?php

namespace Drupal\unomi_profiles\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Url;

/**
 * Renders a table of Unomi profiles.
 *
 * Operations provide links to view the profile and its list of events.
 *
 * Properties:
 * - #profiles: An array of profile objects as returned from a Unomi request.
 *
 * @RenderElement("unomi_profile_list")
 */
class UnomiProfileList extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    return [
      '#profiles' => [],
      '#pre_render' => [
        [$class, 'preRenderProfileList'],
      ],
    ];
  }

  /**
   * Pre-render callback for a profiles list.
   */
  public static function preRenderProfileList($element) {
    $header = [
      ['data' => t('#')],
      ['data' => t('Name')],
      ['data' => t('Segments')],
      ['data' => t('Number Of Visits')],
      ['data' => t('Last visit')],
      ['data' => t('Version')],
      ['data' => t('Operations')],
    ];

    $rows = [];
    foreach ($element['#profiles'] as $profile) {
      $operations = [];
      $operations['view'] = [
        'title' => t('View profile'),
        'url' => Url::fromRoute('unomi_profiles.profile', ['profile_id' => $profile->itemId]),
      ];
      $operations['events'] = [
        'title' => t('View events'),
        'url' => Url::fromRoute('unomi_profiles.list_events', ['profile_id' => $profile->itemId]),
      ];

      $rows[$profile->itemId] = [
        'class' => ['draggable'],
        'data' => [
          'id' => [
            'data' => [
              '#type' => 'link',
              '#url' => Url::fromRoute('unomi_profiles.profile', [
                'profile_id' => $profile->itemId,
              ]),
              '#title' => $profile->itemId,
            ],
          ],
          'name' => $profile->properties->name ?? '---',
          'segments' => implode(', ', $profile->segments),
          'description' => $profile->properties->nbOfVisits ?? '---',
          'last_visit' => $profile->properties->lastVisit ?? '---',
          'version' => $profile->version ?? '---',
          'edit' => [
            'data' => [
              '#type' => 'operations',
              '#links' => $operations,
            ],
          ],
        ],
      ];
    }

    $element = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => [
        'class' => ['unomi-profile-list'],
      ],
      '#empty' => t("No profiles found."),
    ];

    return $element;
  }

}
