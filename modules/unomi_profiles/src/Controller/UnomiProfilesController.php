<?php

namespace Drupal\unomi_profiles\Controller;

use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\unomi_connect\Controller\UnomiControllerBase;

/**
 * Returns responses for Unomi Profiles routes.
 */
class UnomiProfilesController extends UnomiControllerBase {

  /**
   * Builds the response.
   */
  public function index() {

    $profiles = $this->getProfiles();

    $header = [
      ['data' => $this->t('#')],
      ['data' => $this->t('Name')],
      ['data' => $this->t('Segments')],
      ['data' => $this->t('Number Of Visits')],
      ['data' => $this->t('Last visit')],
      ['data' => $this->t('Version')],
      ['data' => $this->t('Operations')],
    ];

    $rows = [];
    foreach ($profiles as $profile) {
      $operations = [];
      $operations['view'] = [
        'title' => $this->t('View profile'),
        'url' => Url::fromRoute('unomi_profiles.profile', ['profile_id' => $profile->itemId]),
      ];
      $operations['events'] = [
        'title' => $this->t('View Events'),
        'url' => Url::fromRoute('unomi_profiles.list_events', ['profile_id' => $profile->itemId]),
      ];

      $rows[$profile->itemId] = [
        'class' => ['draggable'],
        'data' => [
          'id' => [
            'data' => [
              '#type' => 'link',
              '#url' => Url::fromRoute('unomi_profiles.profile', [
                'profile_id' => $profile->itemId,
              ]),
              '#title' => $profile->itemId,
            ],
          ],
          'name' => $profile->properties->name ?? '---',
          'segments' => implode(', ', $profile->segments),
          'description' => $profile->properties->nbOfVisits ?? '---',
          'last_visit' => $profile->properties->lastVisit ?? '---',
          'version' => $profile->version ?? '---',
          'edit' => [
            'data' => [
              '#type' => 'operations',
              '#links' => $operations,
            ],
          ],
        ],
      ];
    }

    $output['states'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => [
        'class' => ['devel-state-list'],
      ],
      'pager' => [
        '#type' => 'pager',
      ],
    ];

    return $output;
  }

  /**
   * Shows a single profile.
   *
   * @param string $profile_id
   *   The Unomi profile ID.
   */
  public function profile(string $profile_id) {
    $result = $this->unomiConnect->makeRequest('GET', '/cxs/profiles/' . $profile_id);
    $profile = $result->getBody()->getContents();

    $build['#title'] = $this->t('Unomi profile @id', [
      '@id' => $profile_id,
    ]);

    $build['data'] = [
      '#type' => 'unomi_connect_json_data',
      '#data' => $profile,
    ];

    return $build;
  }

  /**
   * List the profile events.
   *
   * @param string $profile_id
   *   The Unomi profile ID.
   *
   * @return array
   *   Return the render array.
   */
  public function events(string $profile_id) {
    $events = $this->getEventsByProfile($profile_id);

    // Sort by event timestamp descending.
    usort($events, fn($a, $b) => $b->timeStamp <=> $a->timeStamp);

    $header = [
      ['data' => $this->t('Event ID')],
      ['data' => $this->t('Type')],
      ['data' => $this->t('Date')],
      ['data' => $this->t('Page URL')],
      ['data' => $this->t('Operations')],
    ];

    $rows = [];
    foreach ($events as $event) {

      $operations['edit'] = [
        'title' => $this->t('Details'),
        'url' => Url::fromRoute('unomi_profiles.details_event', ['eventId' => $event->itemId]),
      ];

      $rows[$event->itemId] = [
        'class' => ['draggable'],
        'data' => [
          'id' => $event->itemId,
          'type' => $event->eventType ?? '---',
          'description' => $event->timeStamp ?? '---',
          'version' => $event->target->properties->pageInfo->destinationURL
          ?? $event->source->properties->page->url
          ?? $event->source->properties->pageInfo->destinationURL
          ?? '---',
          'edit' => [
            'data' => [
              '#type' => 'operations',
              '#links' => $operations,
            ],
          ],
        ],
      ];
    }

    $output['#title'] = $this->t('Events for profile @id', [
      '@id' => $profile_id,
    ]);

    $output['states'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes' => [
        'class' => ['devel-state-list'],
      ],
      'pager' => [
        '#type' => 'pager',
      ],
    ];
    $output['#markup'] = count($events) . ' event(s)';

    return $output;
  }

  /**
   * Show event datails.
   *
   * @return array
   *   Return the render array.
   */
  public function detailsEvent() {
    $eventId = \Drupal::routeMatch()->getParameter('eventId');
    $event = ($this->getEventsById($eventId));

    $output['event'] = [
      '#type' => 'unomi_connect_json_data',
      '#data' => $event,
    ];

    return $output;
  }

  /**
   * Get event by id.
   *
   * @param string $id
   *   The event id.
   *
   * @return string
   *   Return json of event.
   */
  public function getEventsById(string $id) {

    /** @var \Drupal\unomi_connect\UnomiConnect $unomiConnect */
    $unomiConnect = \Drupal::service('unomi_connect');
    $search = (object) [
      'offset' => 0,
      'limit' => 100,
      'condition' => (object) [
        'type' => 'eventPropertyCondition',
        'parameterValues' => (object) [
          'propertyName' => 'itemId',
          'comparisonOperator' => 'equals',
          'propertyValue' => $id,
        ],
      ],
    ];

    $result = $unomiConnect->makeRequest('POST', '/cxs/events/search/', $search);
    return json_encode(json_decode($result->getBody()->getContents())->list);
  }

  /**
   * Get events list by profile id.
   *
   * @param string $profileId
   *   The unomi profile id.
   *
   * @return array
   *   Return event collection.
   */
  public function getEventsByProfile(string $profileId) : array {

    /** @var \Drupal\unomi_connect\UnomiConnect $unomiConnect */
    $unomiConnect = \Drupal::service('unomi_connect');
    $search = (object) [
      'offset' => 0,
      'limit' => 300,
      'condition' => (object) [
        'type' => 'eventPropertyCondition',
        'parameterValues' => (object) [
          'propertyName' => 'profileId',
          'comparisonOperator' => 'equals',
          'propertyValue' => $profileId,
        ],
      ],
    ];

    $result = $unomiConnect->makeRequest('POST', '/cxs/events/search/', $search);
    return json_decode($result->getBody()->getContents())->list;
  }

  /**
   * Get all unomi profiles.
   *
   * @return array
   *   Return collection of unomi profiles.
   */
  public function getProfiles(): array {

    /** @var \Drupal\unomi_connect\UnomiConnect $unomiConnect */
    $unomiConnect = \Drupal::service('unomi_connect');
    $search = (object) [
      'offset' => 0,
      // @todo Make the profiles list table click-sortable.
      'sortby' => 'systemProperties.lastUpdated:desc',
      'forceRefresh' => TRUE,
      'condition' => (object) [
        'type' => 'profilePropertyCondition',
        'parameterValues' => (object) [
          'propertyName' => 'itemType',
          'comparisonOperator' => 'equals',
          'propertyValue' => 'profile',
        ],
      ],
    ];

    $result = $unomiConnect->makeRequest('POST', '/cxs/profiles/search/', $search);

    return json_decode($result->getBody()->getContents())->list;
  }

  /**
   * {@inheritDoc}
   */
  public function getModuleName() : string {
    return 'Unomi Profiles';
  }

  /**
   * {@inheritDoc}
   */
  public function getSettingsFormRoute() : string {
    return '';
  }

}
