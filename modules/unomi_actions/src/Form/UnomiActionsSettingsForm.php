<?php

namespace Drupal\unomi_actions\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure unomi_actions settings for this site.
 */
class UnomiActionsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unomi_actions_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['unomi_actions.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['actions_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Actions URI'),
      '#default_value' => $this->config('unomi_actions.settings')->get('actions_uri'),
      '#field_prefix' => $this->config('unomi_connect.settings')->get('base_uri'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('unomi_actions.settings')
      ->set('actions_uri', $form_state->getValue('actions_uri'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
