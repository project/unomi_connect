<?php

namespace Drupal\unomi_actions\Controller;

use Drupal\unomi_connect\Controller\UnomiControllerBase;

/**
 * Returns responses for unomi_actions routes.
 */
class UnomiActionsController extends UnomiControllerBase {

  /**
   * Builds the response.
   */
  public function index() {

    $conditions = $this->getContents(
      $this->config('unomi_actions.settings')->get('actions_uri')
    );

    $header = [
      ['data' => $this->t('#')],
      ['data' => $this->t('Name')],
      ['data' => $this->t('Description')],
      ['data' => $this->t('System Tags')],
      ['data' => $this->t('Operations')],
    ];

    $rows = [];
    foreach ($conditions as $condition) {
      $rows[] = [
        'data' => [
          'id' => $condition->id,
          'name' => $condition->name,
          'description' => $condition->description,
          'SystemTags' => implode(', ', $condition->systemTags),
          'operations' => '---',
        ],
      ];
    }

    $output['table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      'pager' => [
        '#type' => 'pager',
      ],
    ];

    return $output;
  }

  /**
   * {@inheritDoc}
   */
  public function getModuleName() : string {
    return 'Unomi Actions';
  }

  /**
   * {@inheritDoc}
   */
  public function getSettingsFormRoute() : string {
    return 'unomi_actions.settings_form';
  }

}
