<?php

namespace Drupal\unomi_connect\Controller;

/**
 * Unomi controller interface.
 */
interface UnomiControllerInterface {

  /**
   * Get module name.
   *
   * @return string
   *   Return module name.
   */
  public function getModuleName() : string;

  /**
   * Get route to module settings form.
   *
   * @return string
   *   Return route to settings form.
   */
  public function getSettingsFormRoute() : string;

}
