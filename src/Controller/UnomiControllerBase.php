<?php

namespace Drupal\unomi_connect\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\unomi_connect\UnomiConnect;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for unomi_connect routes.
 */
abstract class UnomiControllerBase extends ControllerBase implements UnomiControllerInterface {

  /**
   * The unomi connect service.
   *
   * @var \Drupal\unomi_connect\UnomiConnect
   */
  protected $unomiConnect;

  /**
   * The constructor method.
   *
   * @param \Drupal\unomi_connect\UnomiConnect $unomi_connect
   *   The unomi connect service.
   */
  public function __construct(UnomiConnect $unomi_connect) {
    $this->unomiConnect = $unomi_connect;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('unomi_connect')
    );
  }

  /**
   * Get request content.
   *
   * @param string $uri
   *   The endpoint to request.
   *
   * @return array
   *   Return null or the content.
   */
  public function getContents($uri) : array {

    try {
      $response = $this->unomiConnect->makeRequest('GET', $uri);
      return json_decode($response->getBody()->getContents());
    }
    catch (\Throwable $th) {
      $this->messenger()->addWarning(
        $this->t('Please set the @moduleName uri in <a href="@linkForm">@linkForm</a>.', [
          '@moduleName' => $this->getModuleName(),
          '@linkForm' => Url::fromRoute($this->getSettingsFormRoute())->toString(),
        ])
      );
      return [];
    }
  }

}
