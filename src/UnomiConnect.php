<?php

namespace Drupal\unomi_connect;

use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use stdClass;

/**
 * UnomiConnect service.
 */
class UnomiConnect {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs an UnomiConnect object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Get a client connection.
   *
   * @return \GuzzleHttp\Client
   *   The unomi client connection.
   */
  public function getUnomiClient() : Client {
    $unomiConnectSettings = $this->configFactory->get('unomi_connect.settings');
    $uri = $unomiConnectSettings->get('base_uri');
    $port = $unomiConnectSettings->get('port');
    $baseUri = $uri . ':' . $port;

    $client = new Client(['base_uri' => $baseUri]);
    return $client;
  }

  /**
   * Make Unomi Request.
   *
   * @param string $method
   *   The verb.
   * @param string $uri
   *   The uri.
   * @param object $stdClass
   *   The object to encode json.
   *
   * @return \GuzzleHttp\Psr7\Response
   *   The response.
   */
  public function makeRequest(string $method, string $uri, stdClass $stdClass = NULL) : Response {
    $unomiConnectSettings = $this->configFactory->get('unomi_connect.settings');
    $username = $unomiConnectSettings->get('username');
    $password = $unomiConnectSettings->get('password');

    $response = $this->getUnomiClient()->request($method, $uri, [
      'headers' => [
        'Content-Type' => "application/json",
      ],
      'auth' => [$username, $password],
      'json' => $stdClass,
    ]);

    return $response;
  }

  /**
   * Decodes a response from Unomi which holds a list of data.
   *
   * @param \GuzzleHttp\Psr7\Response $response
   *   The response from Unomi.
   *
   * @return array
   *   The array of data.
   *
   * @throws \Exception
   *   Throws an exception if the response status is not 200.
   */
  public function decodeListResponse(Response $response): array {
    if ($response->getStatusCode() != 200) {
      // @todo change to a better exception class, and add error message.
      throw new \Exception();
    }

    $contents = $response->getBody()->getContents();

    return json_decode($contents)->list ?? [];
  }

}
