<?php

namespace Drupal\unomi_connect\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Render element for displaying JSON data in a textarea.
 *
 * @RenderElement("unomi_connect_json_data")
 */
class JsonData extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = static::class;
    return [
      '#data' => [],
      '#pre_render' => [
        [$class, 'preRenderTextArea'],
      ],
    ];
  }

  /**
   * Pre-render callback for a JSON data element.
   */
  public static function preRenderTextArea($element) {
    $element = [
      '#theme' => 'textarea',
      '#value' => $element['#data'],
      '#attributes' => [
        'id' => 'json',
      ],
      '#attached' => [
        'library' => [
          'unomi_connect/textarea_json',
        ],
      ],
    ];

    return $element;
  }

}
