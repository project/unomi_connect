<?php

namespace Drupal\unomi_connect\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\unomi_connect\UnomiConnect;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Unomi Connect form.
 */
class MakeRequest extends FormBase {

  /**
   * The unomi connect service.
   *
   * @var \Drupal\unomi_connect\UnomiConnect
   */
  protected $unomiConnect;

  /**
   * The constructor.
   *
   * @param \Drupal\unomi_connect\UnomiConnect $unomi_connect
   *   The unomi connect service.
   */
  public function __construct(UnomiConnect $unomi_connect) {
    $this->unomiConnect = $unomi_connect;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('unomi_connect')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unomi_connect_make_request';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#attached']['library'][] = 'unomi_connect/unomi_connect';

    $form['method'] = [
      '#type' => 'select',
      '#title' => 'Request Method',
      '#required' => TRUE,
      '#options' => [
        'POST' => $this->t('POST'),
      ],
    ];

    $form['endPoint'] = [
      '#type' => 'select',
      '#title' => 'End Point',
      '#required' => TRUE,
      '#options' => [
        '/cxs/segments' => $this->t('/cxs/segments'),
        '/cxs/rules' => $this->t('/cxs/rules'),
        '/cxs/definations/conditions' => $this->t('/cxs/definations/conditions'),
        '/cxs/definations/actions' => $this->t('/cxs/definations/actions'),
        '/cxs/profiles' => $this->t('/cxs/profiles'),
      ],
    ];

    $form['json'] = [
      '#type' => 'textarea',
      '#title' => $this->t('JSon'),
      '#required' => TRUE,
      '#attributes' => ['class' => ['request-json']],

    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (mb_strlen($form_state->getValue('json')) < 10) {
      $form_state->setErrorByName('name', $this->t('Message should be at least 10 characters.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $json = $form_state->getValue('json');
      $method = $form_state->getValue('method');
      $endPoint = $form_state->getValue('endPoint');

      $this->unomiConnect->makeRequest($method, $endPoint, json_decode($json));
      $this->messenger()->addStatus($this->t('Request successfully.'));

    }
    catch (\Throwable $th) {
      $this->messenger()->addError($this->t('Error request: @error', ['@error' => $th]));
    }
  }

}
