<?php

namespace Drupal\unomi_connect\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Unomi Connect settings for this site.
 */
class UnomiConnectSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unomi_connect_unomi_connect_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['unomi_connect.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['base_uri'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base URI'),
      '#description' => $this->t("The base URL of the Unomi instance. This will typically be in the form 'https://domain.com/cxs/' and should include the trailing slash."),
      '#default_value' => $this->config('unomi_connect.settings')->get('base_uri'),
    ];
    $form['port'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Port'),
      '#default_value' => $this->config('unomi_connect.settings')->get('port'),
    ];

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $this->config('unomi_connect.settings')->get('username'),
    ];

    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $this->config('unomi_connect.settings')->get('password'),
    ];

    $form['track_script'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Unomi Track Script JS'),
      '#default_value' => $this->config('unomi_connect.settings')->get('track_script'),
      '#rows' => 15,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('unomi_connect.settings')
      ->set('base_uri', $form_state->getValue('base_uri'))
      ->set('port', $form_state->getValue('port'))
      ->set('username', $form_state->getValue('username'))
      ->set('password', $form_state->getValue('password'))
      ->set('track_script', $form_state->getValue('track_script'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
