# Unomi Connect

Unomi Connect is a module that provides a connection API to Apache Unomi. This API is used by the sub modules to consume Apache Unomi Web Services.

This module also provides a form for making requests to Apache Unomi using json text.


## Unomi Connect Settings
![Unomi Connect Settings Form](img/unomi-connect-settings.png)

```
This form is available at /admin/config/services/unomi-connect
```

## Drupal Service

Using unomi_connect drupal service:
```php
/** @var \Drupal\unomi_connect\UnomiConnect $unomiConnect */
$unomiConnect = \Drupal::service('unomi_connect');

// Get a client connection to unomi.
$unomiConnect->getUnomiClient();

// Make Unomi Request.
$unomiConnect->makeRequest(string $method, string $uri, stdClass $stdClass = NULL);
```

## Sub Modules

* Unomi Actions;
* Unomi Conditions;
* Unomi Profiles;
* Unome Rules;
* Unomi Segments.

## MAINTAINERS

### Current maintainers:

 * Thalles Ferreira (thalles) - https://www.drupal.org/user/3589086
